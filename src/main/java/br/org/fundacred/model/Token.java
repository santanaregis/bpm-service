package br.org.fundacred.model;

import br.org.fundacred.enumerator.TokenOriginEnum;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Token {

    private TokenOriginEnum origin;

    private String id;
    private String username;
    private String authorities;
    private String name;
    private String email;
    private String groups;
    private boolean isValid;

    public Token() {
    }

    public Token(boolean isValid) {
        this.isValid = isValid;
    }

    public TokenOriginEnum getOrigin() {
        return origin;
    }

    public void setOrigin(TokenOriginEnum origin) {
        this.origin = origin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}