package br.org.fundacred.enumerator;

public enum TokenOriginEnum {
    PORTAL_ESTUDANTE ("ESTUDANTE"),
    PORTAL_IES ("IES"),
    FILA_DE_TRABALHO ("FILA"),
    NOVA_PLATAFORMA("NOVA_PLATAFORMA");

    private String descricao;

    TokenOriginEnum(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao(){
        return this.descricao;
    }

}