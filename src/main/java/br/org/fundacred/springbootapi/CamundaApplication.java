package br.org.fundacred.springbootapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("br.org.fundacred")
public class CamundaApplication {
    public static void main(String... args) {
        SpringApplication.run(CamundaApplication.class, args);
    }
}
