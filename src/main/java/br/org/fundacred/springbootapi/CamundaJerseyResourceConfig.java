package br.org.fundacred.springbootapi;

import org.camunda.bpm.engine.rest.impl.CamundaRestResources;
import org.camunda.bpm.engine.rest.impl.NamedProcessEngineRestServiceImpl;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/rest")
@Primary
public class CamundaJerseyResourceConfig extends ResourceConfig implements InitializingBean {
    private static final Logger log = LoggerFactory.getLogger(CamundaJerseyResourceConfig.class);

    public void afterPropertiesSet() throws Exception {
        registerCamundaRestResources();
        registerAdditionalResources();
    }

    protected void registerCamundaRestResources() {
        log.info("Configuring camunda rest api.");

        registerClasses(NamedProcessEngineRestServiceImpl.class);
        registerClasses(CamundaRestResources.getResourceClasses());
        registerClasses(CamundaRestResources.getConfigurationClasses());
        register(JacksonFeature.class);

        log.info("Finished configuring camunda rest api.");

    }

    protected void registerAdditionalResources() {
    }
}
