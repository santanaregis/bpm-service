package br.org.fundacred.controller;

import br.org.fundacred.enumerator.TokenOriginEnum;
import br.org.fundacred.interfaces.TokenService;
import br.org.fundacred.model.Token;
import br.org.fundacred.service.TokenServiceImpl;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.history.HistoricTaskInstanceQuery;
import org.camunda.bpm.engine.rest.dto.history.HistoricTaskInstanceDto;
import org.camunda.bpm.engine.rest.dto.history.HistoricTaskInstanceQueryDto;
import org.camunda.bpm.engine.rest.hal.task.HalTaskList;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    TokenService tokenService;

    @Autowired
    TaskService taskService;

    @Autowired
    HistoryService historyService;

    @Autowired
    ManagementService managementService;

    @GetMapping(value = "/{id}")
    public HalTaskList getTaskById(@PathVariable String id, HttpServletRequest httpServletRequest) {
        //Token token = tokenService.validate(httpServletRequest.getHeader("authorization"));
        List<Task> tasks = taskService.createTaskQuery().taskId(id).list();
        return HalTaskList.fromTaskList(tasks, tasks.size());
    }

    @GetMapping(value = "/processInstance/{processInstanceId}")
    public List<HistoricTaskInstanceDto> queryHistoricTaskInstances(@PathVariable String processInstanceId, HttpServletRequest httpServletRequest) {
        //Token token = tokenService.validate(httpServletRequest.getHeader("authorization"));
        List<HistoricTaskInstance> tasks = historyService.createHistoricTaskInstanceQuery().processInstanceId(processInstanceId).list();

        List<HistoricTaskInstanceDto> result = new ArrayList<HistoricTaskInstanceDto>();
        for (HistoricTaskInstance taskInstance : tasks) {
            HistoricTaskInstanceDto taskInstanceDto = HistoricTaskInstanceDto.fromHistoricTaskInstance(taskInstance);
            result.add(taskInstanceDto);
        }
        return result;
    }

    @GetMapping(value = "/userTasks")
    public HalTaskList getUserTasks(HttpServletRequest httpServletRequest) {

        //token sem o prefixo Bearer/JWT
        Token token = tokenService.validate(httpServletRequest.getHeader("authorization"));
        List<Task> tasks = new ArrayList<Task>();

        if(token.isValid()) {
            //tarefas do user = ESTUDANTE
            //tarefas com o assignee igual ao CPF do estudante
            if(token.getOrigin() != null && token.getOrigin().equals(TokenOriginEnum.PORTAL_ESTUDANTE)){
                tasks = taskService.createTaskQuery().taskAssigneeLike("%"+token.getUsername()+"%").list();
                return HalTaskList.fromTaskList(tasks, tasks.size());
            }
            //tarefas do user = PORT
            // AL_IES
            //tarefas com o candidate groups com sigla da ies e do campus do usuário
            //TODO: Verificar como os grupos do usuário virão no token
            else if(token.getOrigin() != null && token.getOrigin().equals(TokenOriginEnum.PORTAL_IES)){
                String[] s = token.getGroups().split(",");
                for (String group: s) {
                    List<Task> temp = taskService.createTaskQuery().taskCandidateGroup(group).includeAssignedTasks().list();
                    if(tasks != null && tasks.size() > 0){
                        tasks.retainAll(temp);
                    }
                    else{
                        tasks = temp;
                    }
                }
                return HalTaskList.fromTaskList(tasks, tasks.size());
            }
            //tarefas do user = FILA_TRABALHO(interno fundacred)
            //tarefas com o candidate groups com grupos do usuario e assignee com id do usuário
            else if(token.getOrigin() != null && token.getOrigin().equals(TokenOriginEnum.FILA_DE_TRABALHO)){
                tasks = taskService.createTaskQuery().taskCandidateGroupIn(Arrays.asList(token.getGroups().split(","))).taskAssigneeLike("%"+token.getUsername()+"%").list();
                return HalTaskList.fromTaskList(tasks, tasks.size());
            }
            //DEFINIR A ORIGEM
            //ESTUDANTE/IES/...?
            else if(token.getOrigin() != null && token.getOrigin().equals(TokenOriginEnum.NOVA_PLATAFORMA)){
                //            NativeTaskQuery().sql("SELECT * FROM " + managementService.getTableName(Task.class)).list();
            }

            List<HashMap<String, String>> responselist = new ArrayList<>();
        }

        return HalTaskList.fromTaskList(tasks, tasks.size());
    }

}
