package br.org.fundacred.controller;

import br.org.fundacred.interfaces.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstanceQuery;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.history.HistoricProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.repository.ProcessDefinitionDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceWithVariablesDto;
import org.camunda.bpm.engine.rest.dto.runtime.StartProcessInstanceDto;
import org.camunda.bpm.engine.rest.exception.RestException;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/process")
public class ProcessController {

    @Autowired
    TokenService tokenService;

    @Autowired
    TaskService taskService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    ManagementService managementService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    HistoryService historyService;

    @GetMapping(value = "/{id}")
    public List<ProcessInstanceDto> getProcessInstance(@PathVariable String id, HttpServletRequest httpServletRequest) {
        //Token token = tokenService.validate(httpServletRequest.getHeader("authorization"));
        List<ProcessInstance> processList = runtimeService.createProcessInstanceQuery().processInstanceId(id).list();

        List<ProcessInstanceDto> dtos = new ArrayList<ProcessInstanceDto>();
        for (ProcessInstance instance: processList){
            dtos.add(ProcessInstanceDto.fromProcessInstance(instance));
        }
        return dtos;
    }

    @GetMapping(value = "/searchProcessInstances")
    public List<HistoricProcessInstanceDto> getProcessInstancesByBusinessKey(@RequestParam("businessKey") String businessKey, @RequestParam("processDefinitionKey") String processDefinitionKey, HttpServletRequest httpServletRequest) {
        //Token token = tokenService.validate(httpServletRequest.getHeader("authorization"));
        HistoricProcessInstanceQuery query = historyService.createHistoricProcessInstanceQuery();
        if(businessKey != null && !businessKey.equals("")){
            query =  query.processInstanceBusinessKeyLike("%" + businessKey + "%");
        }
        if(processDefinitionKey != null && !processDefinitionKey.equals("")){
            query = query.processDefinitionKey(processDefinitionKey);
        }
        List<HistoricProcessInstance> processList = query.orderByProcessInstanceStartTime().desc().list();

        List<HistoricProcessInstanceDto> result = new ArrayList<HistoricProcessInstanceDto>();
        for (HistoricProcessInstance processInstance : processList) {
            HistoricProcessInstanceDto processInstanceDto = HistoricProcessInstanceDto.fromHistoricProcessInstance(processInstance);
            result.add(processInstanceDto);
        }
        return result;
    }

    @GetMapping(value = "/definitions")
    public List<ProcessDefinitionDto> getActiveProcessDefinition(){
        //Token token = tokenService.validate(httpServletRequest.getHeader("authorization"));
        List<ProcessDefinition> definitionsList = repositoryService.createProcessDefinitionQuery().active().latestVersion().list();

        List<ProcessDefinitionDto> dtos = new ArrayList<ProcessDefinitionDto>();
        for (ProcessDefinition definition: definitionsList){
            dtos.add(ProcessDefinitionDto.fromProcessDefinition(definition));
        }
        return dtos;
    }

    @PostMapping(value = "/startProcessInstance/{key}")
    public ProcessInstanceDto startProcessInstance(@PathVariable String key, @RequestBody StartProcessInstanceDto startDto){
        try {
            Map<String, Object> processInstanceVariables = VariableValueDto.toMap(startDto.getVariables(), ProcessEngines.getDefaultProcessEngine(), new ObjectMapper());

            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(key, startDto.getBusinessKey(), processInstanceVariables);
            return ProcessInstanceWithVariablesDto.fromProcessInstance(processInstance);
        }
        catch(Exception e){
            System.err.println(e);
            throw new RestException(Response.Status.INTERNAL_SERVER_ERROR, e, "Error while executing startProcessInstance for "+key);
        }

    }

}
