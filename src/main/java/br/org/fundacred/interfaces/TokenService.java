package br.org.fundacred.interfaces;

import br.org.fundacred.model.Token;

public interface TokenService {

    Token validate(String authorization);
}
