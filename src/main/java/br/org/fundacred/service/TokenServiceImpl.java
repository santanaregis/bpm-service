package br.org.fundacred.service;

import br.org.fundacred.enumerator.TokenOriginEnum;
import br.org.fundacred.interfaces.TokenService;
import br.org.fundacred.model.Token;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SigningKeyResolver;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Map;

@Service
public class TokenServiceImpl implements TokenService {

    private final Base64.Decoder decoder = Base64.getDecoder();

    @Value("${token.signature}")
    private String tokenSignature;

    @Override
    public Token validate(String authorization) {

        try {
            String tokenString = authorization.split(" ")[1];
            //format:'header.body.signature' base64
            String[] tokenSplit = tokenString.split("\\.");

            //validate signature
            Jwts.parser().setSigningKey(tokenSignature.getBytes()).parseClaimsJws(tokenString);

            JSONObject bodyJson = new JSONObject(new String(decoder.decode(tokenSplit[1])));
            Map<String,Object> bodyMap = bodyJson.toMap();

            Token token = new Token();
            if(bodyMap.containsKey("origin")){
                token.setOrigin(TokenOriginEnum.valueOf(bodyMap.get("origin").toString()));
            }
            if(bodyMap.containsKey("id")) {
                token.setId(bodyMap.get("id").toString());
            }
            if(bodyMap.containsKey("sub")){
                token.setUsername(bodyMap.get("sub").toString());
            }
            if(bodyMap.containsKey("auth")) {
                token.setAuthorities(bodyMap.get("auth").toString());
            }
            if(bodyMap.containsKey("name")) {
                token.setName(bodyMap.get("name").toString());
            }
            if(bodyMap.containsKey("email")) {
                token.setEmail(bodyMap.get("email").toString());
            }
            if(bodyMap.containsKey("groups")) {
                token.setGroups(bodyMap.get("groups").toString());
            }

            token.setValid(true);
            return token;
        } catch (NullPointerException | JwtException e) {
            e.printStackTrace();
            return new Token(false);
        }

    }

}